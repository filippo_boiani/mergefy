/*
  SHARED EVENT DATABASE
  TABLES:
  user: it doesn't need explenation...
  event: an event created by user
  document: written by users during an event
  place: an event location
  partecipation: user's partecipation to events

  NOTE:
    1- In mysql we can only have one table with CURRENT_TIMESTAMP as default (or on update value)
    for a timestamp value. (I don't know why though)

    2 - first step, after you have switched on the MySQL server,
    type: /Applications/MAMP/Library/bin/mysql --host=localhost -uroot -proot

    3- A check statement inside a table is parsed but not ignored because it is not supported by mySQL
*/

/* Database cretion */
CREATE DATABASE IF NOT EXISTS shared_events_v1;
USE shared_events_v1;
/* USER */
CREATE TABLE IF NOT EXISTS user (
  id int auto_increment primary key,
  name varchar(100) not null,
  lastname varchar(100) not null,
  born date,
  subscriptiondate timestamp not null,
  type enum('basic','premium','admin') default 'basic',
  profilepicture varchar(300),
  actual_lat decimal(11,8),
  actual_lng decimal(11,8),
  password varchar(300) not null,
  mail varchar(150) not null,
  delated  ENUM('0','1') default '0'
) engine=INNODB;

/* EVENT */
CREATE TABLE IF NOT EXISTS event (
  id int auto_increment primary key,
  name varchar(100) not null,
  place int references place(id) on delete set null,
  creation timestamp not null,
  start timestamp,
  stop timestamp,
  creator int references user(id),
  type int default 0,/*da definire*/
  description varchar(2000), /*da definire*/
  categoryid int
) engine=INNODB;

/* DOCUMENT */
CREATE TABLE IF NOT EXISTS document (
  id int auto_increment primary key ,
  creator int not null references user(id),
  docname varchar(100) default "unknown document",
  event int references event(id),
  public ENUM('0','1') default '0',

) engine=INNODB;

/* PLACE */
CREATE TABLE IF NOT EXISTS place (
  id int auto_increment primary key,
  lat decimal(11,8),
  lng decimal(11,8),
  name varchar(100) not null,
  address varchar(200) not null,
  cap varchar(10),
  city varchar(50) not null,
  nation varchar(50) not null default "Italy"
) engine=INNODB;

/* PARTECIPATION */
CREATE TABLE IF NOT EXISTS partecipation (
  id int auto_increment primary key,
  event_id int not null references event(id),
  user_id int not null references user(id),
  status ENUM('accepted','declined','waiting') default 'waiting'
) engine=INNODB;


/******************** STORED PROCEDURES **********************/
/* 1 - insertUser()
    Insert a User
*/
DELIMITER |
CREATE PROCEDURE insertUser(IN name varchar(100),
 IN lastname varchar(100),
 IN born date, IN type int,
 IN profile varchar(300),
 IN password varchar(300),
 IN mail varchar(150),
 OUT id int)
BEGIN
  IF type = 1 THEN
    insert into user(name, lastname, born, subscriptiondate, type, profilepicture, password, mail)
      values(name, lastname, born, CURRENT_TIMESTAMP(), "premium", profile, password, mail);
  ELSEIF type = 2 THEN
    insert into user(name, lastname, born, subscriptiondate, type, profilepicture, password, mail)
      values(name, lastname, born, CURRENT_TIMESTAMP(), "admin", profile, password, mail);
  ELSE
    insert into user(name, lastname, born, subscriptiondate, profilepicture, password, mail)
      values(name, lastname, born, CURRENT_TIMESTAMP(), profile, password, mail);
  END IF;
  SELECT LAST_INSERT_ID() into id;
  SELECT LAST_INSERT_ID() AS "last user insert id ";
END |
DELIMITER ;

/* Test insertUser() */
call insertUser("Riccardo", "Sibani", "1994-09-24", 2, "....", "hashfunction(password+salt)", "riccardo@mail.com", @id);
select @id;

/****** STORED PROCEDURES TRIALS *******/
DELIMITER |
CREATE PROCEDURE getData(OUT TotPrenotazioni int)
BEGIN
	DECLARE b timestamp default 0;
	SELECT born INTO b FROM user WHERE (id=1);
	SELECT year(CURRENT_TIMESTAMP()) - year(b);
END |
DELIMITER ;

call getData(@x);

/******************** TRIGERS **********************/

/* 1 - check_age
    Check the user date of birth
*/
DELIMITER //
create trigger check_age
BEFORE INSERT ON shared_events_v1.user
FOR EACH ROW
BEGIN
	IF (NEW.born IS NULL) OR ( (YEAR(CURRENT_TIMESTAMP()) - YEAR(NEW.born) - (DATE_FORMAT(CURRENT_TIMESTAMP(), '%m%d') < DATE_FORMAT(NEW.born, '%m%d'))) < 14 )
  THEN
      SIGNAL sqlstate '45000' set message_text = "Age must be more than 14 yo";
	END IF;
END //
DELIMITER ;

/* 2 - check_event_creation
    Check the if the creation date is correct
    (it can be omitted since we use a stored procedure and set creation with current_timestamp)
*/
DELIMITER //
create trigger check_event_creation
BEFORE INSERT ON shared_events_v1.event
FOR EACH ROW
BEGIN
	IF (NEW.creation IS NULL) OR NEW.creation > CURRENT_TIMESTAMP() )
  THEN
      SIGNAL sqlstate '45000' set message_text = "Invalid creation date";
	END IF;
END //
DELIMITER ;


/* Tests for check_age */
insert into user(name, lastname, born, subscriptiondate,password, mail) values("ciccio", "prova1", CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), "bellaaaaaa", "bellllla@mail.com");
insert into user(name, lastname, subscriptiondate,password, mail) values("ciccio", "prova1", CURRENT_TIMESTAMP(), "bellaaaaaa", "bellllla@mail.com");

/******************** VIEWS **********************/

/* TODO */

/******************** GRANT PERMISSIONS **********************/
CREATE USER 'user'@'localhost' IDENTIFIED BY 'user-password';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin-password-3h5CHx34t2D';

/* TODO */
