/**
 * Created by filippoboiani on 29/01/16.
 */
$(document).ready(function(){

    // Set te object to send
    var formData = {
        name: "",
        lastname: ""
    };

    console.log(formData);
    //controller.php?controller=user&action=registration

    $('input.submit').on( 'click', function(e){
        console.log("click");
        e.preventDefault();
        // Get all data and crete the object
        var form = $("#registration");
        console.log(form);

        /*
            Crete object from a form: it simply maps name and values.
         */
        var data = {};
        form.serializeArray().map(function(x){data[x.name] = x.value;});
        console.log(data);
        //var serial = form.serialize();
        /*formData.action = "insert";
        formData.controller = "user";
        formData.name = $("#name").val();
        formData.lastname = $("#lastname").val();*/
        //console.log(serial);

        return sendData( data );

    });

    function sendData(dataObject) {

        console.log(dataObject);

        /*
        param()
        * Create a serialized representation of an array, a plain object, or a jQuery object suitable
        * for use in a URL query string or Ajax request. In case a jQuery object is passed, it should
        * contain input elements with name/value properties.
        * */
        data = $.param( {action: "insertUser", controller: "user", param:  dataObject} );

        console.log("data");
        console.log(data);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "handler.php", //you can pass the type of action also response.php?action=typeofaction
            data: data,
            success: function(data) {
                console.log("Success");
                console.log(data);
            },
            error: function(data){
                console.log(data);
            }
        });
        return false;
    }
});