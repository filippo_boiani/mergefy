<?php

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists

        //echo $_POST["action"]." ".$_POST["controller"];
        //echo $_POST["name"]." ".$_POST["lastname"];
        spl_autoload_register('my_autoloader');
        callHook();

    }
}

//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function callHook() {


    $controller = $_POST["controller"];
    $action = $_POST["action"];
    //$queryString = $urlArray; //params
    $param = $_POST["param"];

    //$param = json_decode($param);

    /*
     * Try to call a specific parameter. We'll send the whole object to the specific controller and
     * it has to map Keys and Values (because it is the one who knows what is necesserary to create a specific Model Object.
     */
    echo $param["name"];
    //echo "(controller) ".$controller." - ";
    //echo "(action) ".$action." - ";

    //String operations on Controller's name
    $controllerName = $controller;
    $controller = ucwords($controller);
    $controller .= 'Controller';  // example: UserController

    //echo "(controller) ".$controller." - ";

    //String Operation on model's name
    $model = ucwords(rtrim($controllerName, 's'));

    echo "(model) ". $model." - ";
    // Create an instance of a concrete controller (that has the same constructor of the general class Controller).
    $dispatch = new $controller($model,$controllerName,$action);

    if ((int)method_exists($controller, $action)) {
        echo "(call action) - ";
        // We send the entrire object as agument of the action of the dispatch
        call_user_func_array( array($dispatch,$action), $param );
    } else {
        echo json_encode(array("error" =>1, "message"=> "Impossible to create class"));

    }
}



function my_autoloader($class) {
    include 'model/' . $class . '.class.php';
    include 'controller/'. $class . '.class.php';
}