<?php
/**
 * Created by IntelliJ IDEA.
 * User: filippoboiani
 * Date: 31/01/16
 * Time: 21:25
 */

class Controller
{

    protected $_model;
    protected $_controller;
    protected $_action;

    //protected $_template;

    function __construct($model, $controller, $action)
    {
        echo "(constructor) - ";
        spl_autoload_register('my_autoloader'); //find a way to call this method only once
        //echo $model." ".$controller." ".$action;

        $this->_controller = $controller;
        $this->_action = $action;
        $this->_model = $model;
        echo "(model ?) ".$this->$model." - ";
        $this->$model = new $model; //$model becomes a concrete domain object
        echo "(model ?) - ".$this->_model;
        //$this->_template = new Template($controller,$action);

    }

    function set($name, $value)
    {
        //$this->_template->set($name,$value);

        //set the view
    }

    function __destruct()
    {
        // $this->_template->render();

        //render view
    }



    function my_autoloader($class) {
        include '../model/' . $class . '.class.php';
    }

}

