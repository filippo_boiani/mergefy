<?php

/**
 * Class Position
 *
 * Maintain latitude and longitude of a point a allows to find the distance between two points.
 *
 */
class Position {
  /**
   * @var $latitude
   * @var $longitude
   */
  private $latitude, $longitude;

  /**
   * Position constructor.
   * @param $lat
   * @param $lng
     */
  public function __construct($lat, $lng){
    echo "Test position<br>";
    $this->latitude = $lat;
    $this->longitude = $lng;
    echo $this->latitude." <br>". $this->longitude."<br>";
  }

  /**
   * @param $lat
   */
  public function setLat($lat){
    $this->latitude = $lat;
  }

  /**
   * @param $lng
   */
  public function setLng($lng){
    $this->longitude = $lng;
  }

  /**
   * @param $lat
   * @param $lng
     */
  public function setCoordinates($lat, $lng){
    $this->latitude = $lat;
    $this->longitude = $lng;
  }

  /**
   * @return mixed
   */
  public function getLat(){
    return $this->latitude;
  }

  /**
   * @return mixed
   */
  public function getLng(){
    return $this->longitude;
  }

  /**
   * @return array
   */
  public function getCoordinates(){
    return array( "lat"=>$this->latitude, "lng"=> $this->longitude );
  }

  /**
   * @param Position $pos
   * @return float
   *
   * Return the distance in Meters between to points.
     */
  public function calculateDistance(Position $pos ){
    $earthRadius = 6371000;

    $latFrom = deg2rad($this->getLat());
    $lonFrom = deg2rad($this->getLng());
    $latTo = deg2rad($pos->getLat());
    $lonTo = deg2rad($pos->getLng());

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
      cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return round($angle * $earthRadius, 2, PHP_ROUND_HALF_UP);
  }


  /**
   * @param Position $position
   * @param $radius
   * @return bool
   *
   * Return true if the distance is lower than the value specified in Meters
     */
  public function isInRadius(Position $position, $radius ){

    $distance = $this->calculateDistance($position);
    return $distance <= $radius;
  }

}

