<?php

/**
 * Created by IntelliJ IDEA.
 * User: filippoboiani
 * Date: 29/01/16
 * Time: 09:06
 */
class Utility
{

    /**
     * function setReporting()
     *
     * Used to enable and disable warnings and errors.
     * It is particularly useful during app testing and refactoring and in development time as well.
     *
     * This function checks a global variable called DEVELOPMENT_ENVIRONMENT contained in the file config.php
     */
    function setReporting() {
        if (DEVELOPMENT_ENVIRONMENT == true) {
            error_reporting(E_ALL);
            ini_set('display_errors','On');
        } else {
            error_reporting(E_ALL);
            ini_set('display_errors','Off');
            ini_set('log_errors', 'On');
            ini_set('error_log', ROOT.DS.'tmp'.DS.'logs'.DS.'error.log');
        }
    }


    //It is highly recommend not to use the __autoload() function but use spl_autoload_register()
    /**
     * function __autoload()
     *
     * This function is defined in the PHPDoc and is useful when it comes to load a not-defined class.
     * Therefore, we can say that is used to enable class auto-loading.
     *
     * @param $className
     */
    /*function __autoload($className) {
        if (file_exists("....)) {
        } else {
            // Error Generation Code Here
        }
    }*/


    //It must be moved to a file accessed globally
    /**
     * classAutoLoader() function
     *
     * @param $class
     */
    public function classAutoLoader($class) {
        $class = strtolower($class);
        $classFile = $_SERVER['DOCUMENT_ROOT'] . '/include/class/' . $class . '.class.php';
        if (is_file($classFile) && !class_exists($class)){
            include $classFile;
        }
    }
    //spl_autoload_register("classAutoLoader");


    //So a namespace is like a pointer to a file path where you can find the source of the function you are working with
}