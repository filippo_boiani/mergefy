<?php

/**
 * Class Event
 *
 * User: Filippo Boiani
 * Date: 28/01/16
 * Time: 10:17
 */
class Event
{
    private $id;
    private $name;
    private $description;
    private $creator;
    private $date_creation;
    private $date_start;
    private $date_end;
    private $type;
    private $image;

    /**
     * Event constructor.
     * @param $id
     * @param $name
     * @param $description
     * @param $creator
     * @param $date_creation
     * @param $date_start
     * @param $date_end
     * @param $type
     * @param $image
     */
    public function __construct($name, $description, $creator, $date_creation, $date_start, $date_end, $type, $image)
    {
        $this->name = $name;
        $this->description = $description;
        $this->creator = $creator;
        $this->date_creation = $date_creation;
        $this->date_start = $date_start;
        $this->date_end = $date_end;
        $this->type = $type;
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * @param mixed $date_start
     */
    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    /**
     * @return mixed
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

    /**
     * @param mixed $date_end
     */
    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }




}